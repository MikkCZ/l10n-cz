---
layout: post
title: Převod L10N.cz pod spolek OpenAlt
tags: domena
---

Od roku 2011 byl projekt L10N.cz [zaštiťován Sdružením Ubuntu pro Českou republiku]({% link _posts/2011-03-14-domena-l10ncz.html %}). Bohužel mezitím aktivita sdružení značně poklesla a to se projevilo např. při řešení [havárie serveru]({% link _posts/2019-08-13-havarie-serveru-s-wiki-a-emailovou-konferenci.md %}) loni v létě, kdy jsme přišli o e-mailovou konferenci a dočasně i o wiki.

Česká (a slovenská) komunita okolo serveru Ubuntu.cz sice od té doby zase roste, nicméně sdružení jako právní entita už efektivně nefunguje. Proto jsem se letos v červnu dohodl se [spolkem OpenAlt z.s.](https://www.openalt.org/), který nyní přijal projekt L10N.cz za svůj a bude zajišťovat jak registraci domény, tak hosting pro wiki.

Děkuji sdružení i komunitě Ubuntu za jejich dosavadní podporu, a samozřejmě i spolku OpenAlt za nabídnutou pomoc.

Michal Stanke
