---
layout: page
title: Kontakty
permalink: /kontakty/
order: 1
---

### E-mailová konference
Naše překladatelská konference se [nachází na serveru OpenAltu](https://lists.openalt.org/wws/info/diskuze-l10n-cz) a její e-mailová adresa je [diskuze-l10n-cz@lists.openalt.org](mailto:diskuze-l10n-cz@lists.openalt.org).

### On-line diskuze
- [Matrix](https://matrix.to/#/#l10ncz:matrix.org)

### Správci projektu:
- Petr Kovář - *pknbe zavináč volny.cz* <br>
- Michal Stanke - *mstanke zavináč mozilla.cz*

### Původní autor projektu:
- Václav Čermák - *vaclav.cermak zavináč gmail.com*

### Sponzor domény a infrastruktury l10n.cz:
- [spolek OpenAlt z.s.](https://www.openalt.org/) - *spravci zavináč lists.openalt.org*

### Autor srovnávacího slovníku:
- Martin Böhm - jabber: *mhb zavináč jabber.cz*

### Autor loga l10n.cz:
- Jan Tojnar - *jtojnar zavináč gmail.com*

### Autor ikony „favicon“ l10n.cz:
- Marek Černocký - *marek zavináč manet.cz*

[Seznam registrovaných přispěvatelů](https://wiki.l10n.cz/Speci%C3%A1ln%C3%AD:U%C5%BEivatel%C3%A9) na [wiki.l10n.cz](https://wiki.l10n.cz/).
