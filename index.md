---
layout: page
title: Překladatelská komunita L10N.cz
hide: true
---

### Cíle projektu a překladatelské komunity L10N.cz

- [**Získávání nových zájemců o překlad**]({% link _pages/zapojte-se.md %})
- [**Šíření povědomí o českých překladech**](https://wiki.l10n.cz/Jak_ps%C3%A1t_na_web_L10N.cz)
- Zjednodušení práce na překladech a odstranění dvojitých překladů
- Sjednocení [**terminologie**](https://wiki.l10n.cz/P%C5%99ekladatelsk%C3%BD_slovn%C3%ADk) v oblasti otevřeného a svobodného softwaru
- Zvyšování úrovně českých překladů
- Spolupráce mezi [**překladatelskými týmy**](https://wiki.l10n.cz/P%C5%99ekladatelsk%C3%A9_t%C3%BDmy) z různých projektů

### Kde začít

Každý, kdo je ochotný přispět svými zkušenostmi, nebo začít překládat, je zde vítán. 

- Mezi [**návody pro překladatele**](https://wiki.l10n.cz/Kategorie:N%C3%A1vody) najdete pár tipů do začátku nebo když si nebudete vědět rady.
- Stránka [**Jak se zapojit**]({% link _pages/zapojte-se.md %}) vám pomůže zorientovat se, pokud chcete rozšiřovat tuto wiki.
